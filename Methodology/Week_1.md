# Weekly Logbook - Week 1

## Agenda

- Course introduction
- Create Subsystems and Team

## Objectives

- Make automated airship with sprayer fly
- Understand the requirements of the course and project
- Able to participate with the Team and Subsystems

## Problem and decision taken

- No problem encounter
- Asking the detail of the project to the respective supervisor and Dr Salah

## Impact

- Everyone from our subsytem get more clarity about this project and get know what happen in this week

## Description

In the first week, all student has been introduced by Dr Salahuddin as our main instructor for this Aerospace Design Project. Mr. Fiqri and Mr. Azizi appoints as our supervisor through the entire courses. The introduction to the aims of the project were told by Mr. Fiqri. 

The subsystems will be the group named as the all the student will be divided by following their capabilities and interest. The subsystem teams are:

- Flight system integration
- Design
- Simulation
- Propulsion & Power
- Control Systems
- Payload (Agricultural Sprayer)
