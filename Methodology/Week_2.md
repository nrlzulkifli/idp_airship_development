# Weekly Logbook - Week 2

## Agenda

- Divide students into groups according to their interest and capability (based on personality quiz & google form)
- Produce Gantt Chart for the subsystem entire 14 weeks of work
- Gitlab Workshop (by Mr. Azizi)

## Objectives

- Able to use and apply the knowledge of using Gitlab
- Explore Gitlab and Markdown Language
- Discuss the project aims with the subsystem
- Produce Gantt Chart

## Problem and decision taken

- Fully participate with subsystem and team decision
- Learn with others coursemate about Gitlab
- Complete the task 1 and task 2 

## Impact

- Learned new skills with Markdown
- Everyone from our subsytem get more clarity about this project and get know what happen in this week

## Description

Dr Salah had already divided us into groups according to our interest and capability. Personality test had been given to us to determine our preferences. The groups were consists of 7 main groups od Subsystems which are Flight Integration, Control Systems, Propulsion, Design, Simulation and Payload (Sprayer). Then, another teams on the right side is created based on these 7 susbsystems group, which are a combination of students from subsystems group and the task is specified for the progress report, presentation and etc.

Gantt chart is made based on the milestone target. The first milestone target is to have a first fly test for a medium scale airship on the week of 8 and the second miles stone is to have a flight test for a bigger scale Airship with and without payloads on the week of 12 and 14.

A workshop has been made by Mr. Azizi Malek as to introduced and give a basic training of GitLab. After the class, a task named task 2 (self- introduction) has been assign to do individually in order to see understanding and capability to do the task. The main objective of this task is to develop a personal profile and to use all the basics MarkDown Language.
