# Weekly Logbook - Week 3

## Agenda

- Propulsion Test using previous Motor of PHSAU
- Present Gantt Chart
- Regroup in team to appoint a checker, process monitor, recorder and coordinator.
- Complete the engineeering Logbook

## Objectives

**Thrust Test**
- Determine whether or not the motor is still operational
- Investigate the capabilities of the motor and the esc with a different length of propeller that has previously been employed

## Problem and decision taken

**Problem**
1. Difficulties in determining the diameter of the propeller used to propel the airship. As a result, we put 17-inch and 23-inch propellers on a sample motor in the propulsion lab, but the motor couldn't handle the 23-inch propeller and burned

1. Need to obtain the design parameters from Flight Simulation Team

**Decision Taken**
1. We've decided not to deal with the 23-inch propeller any longer because it will cause damage at only approximately half throttle

1. We are suggesting the propeller size is 17 inch with 75% throttle

1. Create a proper timetable for roles for each member

## Impact

- Learned new skills and increase knowledge with the propulsion system as in the thrust test
- Everyone from our subsytem get more clarity about this project and get know what happen in this week

## Description

A thurst test experiment has been conducted by Propulsion Subsytems Group under supervision Dr Ezanee in the Propulsion Lab H2.3. The main objective of this thurst test experiment is to see whether the motor is still functioning or not, the capabilities of the motor and the esc with a difference length of propeller that has been used before. All the setup has been conducted by using RCBenchmark.
