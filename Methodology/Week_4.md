# Weekly Logbook - Week 4

## Agenda

- Try to find drag of the airship using "trolly test"
- Discuss design parameter obtained by Sprayer Team and Flight Simulation Team
- Test the functionality of the motors

## Objectives

- Try the "Trolley Test" to find the force (thrust)
- Propose other techniques to obtain the force
- Obtain the design parameter
- Make sure the motor functioning well
- Identify the burned/not function motor

## Problem and decision taken

**Problem**
1. Difficulties to find drag of the airship using trolly but the result obtained was not valid due to unconstant force applied

1. Test the functionality of the motors and data obtained were recorded


## Impact

- We had familiarized ourselves with the RC Benchmark system as well as the testing methods
- Everyone from our subsytem get more clarity about this project and get know what happen in this week

## Description

Propulsion subsystems performed the thurst test using the RCBenchmark for all the motors and the ESC that has been used before. The test was undersupervision Dr Ezanee and he also give an advise and some reference paper regarding the Airship Development for the groups. There are several discussion regarding the thrust test, some of the topic that might seems very small but its actually good to know; which the motor rotation is not based on propeller disk shape, but it was the ESC connection. This issue were discussed to get a right direction of the thrust when the test being done.
