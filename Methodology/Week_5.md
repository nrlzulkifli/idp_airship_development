# Weekly Logbook - Week 5

## Agenda

- Went to the propulsion lab to test out old motors with similar specifications Tarot 4114 320KV
- Joined the team meeting to know others subsystems progress

## Objectives

- Propose other techniques to obtain the force
- Obtain the design parameter
- Make sure the motor functioning well
- Identify the burned/not function motor

## Problem and decision taken

**Problem**
1. Try to find drag of the airship using trolly but the result obtained was not valid due to unconstant force applied

1. Test the functionality of the old motors which have almost similar specification with Tarot 4114 320KV and data obtained were recorded.

**Decision Taken**
1. Dr Ezanee give an advise and some reference paper regarding the Airship Development for the groups

1. Dr. Ezanee gave a brief explanantion on how we should be able to find the thrust that is required from the equilibrium of forces of the airship


## Impact

- We had familiarized ourselves with the RC Benchmark system as well as the testing methods
- Everyone from our subsytem get more clarity about this project and get know what happen in this week

## Description

Propulsion subsystems performed the thurst test using the RCBenchmark for all the motors and the ESC that has been used before. The test was undersupervision Dr Ezanee and he also give an advise and some reference paper regarding the Airship Development for the groups. There are several discussion regarding the thrust test, some of the topic that might seems very small but its actually good to know; which the motor rotation is not based on propeller disk shape, but it was the ESC connection. This issue were discussed to get a right direction of the thrust when the test being done.
