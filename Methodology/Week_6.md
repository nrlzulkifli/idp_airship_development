# Weekly Logbook - Week 6

## Agenda

- Devided our subsystems into two group for Task Week 6 which is:

- Team A = Going to lab
- Team B = Online Task

## Objectives

- Test second motor using DC Power Supply (16V) and also Battery (15.8V)
- Find the error
- Make sure the motor functioning well
- Identify the burned/not function motor

## Problem and decision taken

**Problem**
1.  Error in troubleshooting current problem due to poor connection


**Decision Taken**
1. Identify the error: crocodile clips that were causing the problem but actually it was the loose connection on the ESC wire.

1. Prepared an excel sheet to calculate: Thrust ( from velocity at 0-10 m/s), Power ( from velocity at 0-10 m/s), Maximum Power Available

1. Removed the outliers from the previous graph for the old motor data

## Impact

- We had familiarized ourselves with the RC Benchmark system as well as the testing methods
- Everyone from our subsytem get more clarity about this project and get know what happen in this week

## Description

We tested second motor using DC Power Supply (16V) and also Battery (15.8V) with ESC Number 4. Unfortunately, we encountered an error in troubleshooting current problem due to poor connection. Initially we thought that it was the crocodile clips that were causing the problem but actually it was the loose connection on the ESC wire. Coming back into the propulsion lab after about 30 minutes, we were able to identify the source of error which is the loose connection like our loose brains. After all troubleshoot, we were able to obtain 1200+- gf of thrust when given 100% of throttle with the battery (15.8V) and 17 inch propeller. Maximum Current was about (9A). Objectives achieved.
