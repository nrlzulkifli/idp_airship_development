# IDP_Airship_Development

**Detail**

Name: [Nurul Ain Izzati Binti Zulkifli](https://gitlab.com/nrlzulkifli/about-me.git)

Matric No.: 196107

Course: EAS 4947 - Integrated Design Project 

Subsystems: Propulsion & Power

Team: 7

Project Name: Airship with an Agricultural Sprayer

## Content
1. Abstract

1. Introduction

1. Literature Review

1. Methodology (Weekly Report)

- [Week 1](https://gitlab.com/nrlzulkifli/idp_airship_development/-/blob/main/Methodology/Week_1.md)
- [Week 2](https://gitlab.com/nrlzulkifli/idp_airship_development/-/blob/main/Methodology/Week_2.md)
- [Week 3](https://gitlab.com/nrlzulkifli/idp_airship_development/-/blob/main/Methodology/Week_3.md)
- [Week 4](https://gitlab.com/nrlzulkifli/idp_airship_development/-/blob/main/Methodology/Week_4.md)
- [Week 5](https://gitlab.com/nrlzulkifli/idp_airship_development/-/blob/main/Methodology/Week_5.md)
- [Week 6](https://gitlab.com/nrlzulkifli/idp_airship_development/-/blob/main/Methodology/Week_6.md)
- Week 7
- Week 8
- Week 9
- Week 10
- Week 11
- Week 12
- Week 13
- Week 14

